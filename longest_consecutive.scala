object LongestConsec {

  def longestConsec(strarr: Array[String], k: Int): String = {
    if (strarr.isEmpty || k <= 0 || k > strarr.length) ""
    else {
      (for (subset <- strarr.sliding(k)) yield subset.flatten)
        .maxBy(_.length)
        .mkString
    }
  }
}